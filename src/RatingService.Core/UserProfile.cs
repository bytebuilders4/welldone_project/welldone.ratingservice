﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Core
{
    public class UserProfile
    {
        public string? Phone { get; set; }
        public required string Email { get; set; }

    }
}
