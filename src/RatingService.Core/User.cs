﻿
namespace RatingService.Core
{
    public class User : BaseEntity<Guid>
    {
        public required string Username { get; set; }
        public double? CustomerRating { get; set; }
        public int? CountCustomerRatings { get; set; }
        public double? ArtistRating { get; set; }
        public int? CountArtistRating { get; set; }
        public required UserProfile UserProfile { get; set; }

    }
}
