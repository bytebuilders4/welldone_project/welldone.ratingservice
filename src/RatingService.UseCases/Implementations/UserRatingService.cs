﻿using Microsoft.EntityFrameworkCore;
using RatingService.Core;
using RatingService.DaraAccess;
using RatingService.UseCases.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RatingService.UseCases.Implementations
{
    public class UserRatingService : IUserRatingService
    {
        private readonly RatingDataContext _ratingDataContext;

        public UserRatingService(RatingDataContext ratingDataContext)
        {
            _ratingDataContext = ratingDataContext;
        }
        public async Task AddNewRatingForArtist(Guid id, int rating)
        {
           var user = await _ratingDataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            var calculatedRating = user.ArtistRating;
            var countRating = user.CountArtistRating;
            calculatedRating = ((calculatedRating*countRating) + rating) / (countRating + 1);
            user.ArtistRating = calculatedRating;
            user.CountArtistRating = countRating + 1;
            _ratingDataContext.Users.Update(user);
            await _ratingDataContext.SaveChangesAsync();
        }

        public async Task AddNewRatingForCustomer(Guid id, int rating)
        {
            var user = await _ratingDataContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            var calculatedRating = user.CustomerRating;
            var countRating = user.CountCustomerRatings;
            calculatedRating = ((calculatedRating * countRating) + rating) / (countRating + 1);
            user.CustomerRating = calculatedRating;
            user.CountCustomerRatings = countRating + 1;
            _ratingDataContext.Users.Update(user);
            await _ratingDataContext.SaveChangesAsync();
        }

        public async Task<List<User>> GetAll()
        {
            return await _ratingDataContext.Users.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<List<User>> GetAllArtistRating()
        {
           return await _ratingDataContext.Users.Where(r=>r.ArtistRating != 0).OrderBy(x=>x.ArtistRating).ToListAsync();
        }

        public async Task<List<User>> GetAllCustomersRating()
        {
            return await _ratingDataContext.Users.Where(r => r.CustomerRating != 0).OrderBy(x => x.CustomerRating).ToListAsync();
        }

        public async Task<User> GetByID(Guid id)
        {
            return await _ratingDataContext.Users.FirstOrDefaultAsync(x=>x.Id == id);
        }
    }
}
