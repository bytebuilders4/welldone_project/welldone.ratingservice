﻿using RatingService.Core;


namespace RatingService.UseCases.Interfaces
{
    public interface IUserRatingService
    {
        Task<User> GetByID(Guid id);

        Task<List<User>> GetAll();

        Task<List<User>> GetAllCustomersRating();

        Task<List<User>> GetAllArtistRating();

        Task AddNewRatingForCustomer(Guid id, int rating);

        Task AddNewRatingForArtist(Guid id, int rating);
    }
}
