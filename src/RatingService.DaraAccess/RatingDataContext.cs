﻿using Microsoft.EntityFrameworkCore;
using RatingService.Core;

namespace RatingService.DaraAccess
{
    public class RatingDataContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public RatingDataContext()
        {
            Database.EnsureCreated();
        }

        public RatingDataContext(DbContextOptions<RatingDataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql();
        }

    }
}
